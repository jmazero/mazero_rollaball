﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public GameObject player;
    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        //subtracting xyz of camera from ball
        offset = transform.position - player.transform.position;
    }

    // Update is called once per frame
    //Lets ball carry out physics before we change the position of the camera
    void LateUpdate()
    {
        //adding the offset to the players position, which is where the camera should be
        transform.position = player.transform.position + offset;
    }
}
