﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    private Rigidbody rb;
    public float speed;
    public Text countText;
    private int count;
    public Text winText;

    public int jumpForce;

    public GameObject floor;

    // Start is called before the first frame update
    void Start()
    {
        count = 0;
        rb = GetComponent<Rigidbody>();
        winText.text = "";
    }

    // Update is called once per frame
    //FixedUpdate is calculated when physics is going on
    void FixedUpdate()
    {
        Move();
    }

    private void Update()
    {
        Jump(); 
    }

    void Move()
    {
        //returns value between 0-1 depending on which way player is moving
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        //add the getaxis values to the movement
        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);
        //move the rigidbody
        rb.AddForce(movement * speed);

    }

    void Jump()
    {
        //create vector that will push player in the y axis
        Vector3 jump = new Vector3(0f, 350f, 0f);

        //add the force to the movement if player presses space
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(jump);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        //if player runs into gameobject tagged "Pick Up", then destroy it, add to count, and redisplay count text
        if (other.gameObject.CompareTag("Pick Up")) {
            other.gameObject.SetActive(false);
            count += 1;
            setCountText();

            //every time the player runs into Pick Up, change the material color of the floor
            Color randColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
            floor.GetComponent<Renderer>().material.color = randColor;
        }
    }

    void setCountText()
    {
        countText.text = "Count: " + count.ToString();
        //if the player has collected all pickups, then display win text.
        if (count >= 12) {
            winText.text = "You Win!";
        }
    }

}
